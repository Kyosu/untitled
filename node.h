//
// Created by kyosu on 11/25/21.
//

#ifndef UNTITLED_NODE_H
#define UNTITLED_NODE_H

namespace nodes{
    template <typename T>
    struct node{
    public:
        T key;
        node* next;
        node(T value){
            key = value;
            next = this;
        };

        T operator[](size_t position){
            if (position==0){
                return this->key;
            } else {
                return this->next[position-1];
            }
        }
    };

    template <typename T>
    void appendNode(node<T>* &currentNode, T value){
        if (currentNode == currentNode->next){
            auto* newNode = new node<T>(value);
            currentNode->next = newNode;
        } else {
            appendNode(currentNode->next, value);
        }
    };

    template <typename T>
    void insertNode(node<T>* &currentNode, T value, size_t position){
        switch (position) {
            case 0:
            {
                if (!currentNode) throw "prependNode() Error";
                auto* newNode = new node<T>(value);
                newNode->next = currentNode;
                currentNode = newNode;
                break;
            }
            case 1:
            {
                auto* newNode = new node<T>(value);
                auto* nextNode = currentNode->next;
                currentNode->next = newNode;
                newNode->next = nextNode;
                if (newNode->next == currentNode) newNode->next = newNode;
                break;
            }
            default:
            {
                insertNode(currentNode->next, value, position-1);
                break;
            }
        }


    };

    template <typename T>
    node<T>* removeNode(node<T>* &currentNode, size_t position){
        switch (position) {
            case 0:
            {
                auto* thisNode = currentNode;
                currentNode = currentNode->next;
                delete thisNode;
                break;
            }

            case 1:
            {
                auto* nextNode = currentNode->next->next;
                delete currentNode->next;
                currentNode->next = nextNode;
                break;
            }

            default:
            {
                removeNode(currentNode->next, position-1);
                break;
            }
        }
    };


    template <typename T>
    node<T>* delList(node<T> &begin);




}


#endif //UNTITLED_NODE_H
